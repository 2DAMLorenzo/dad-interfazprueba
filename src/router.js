import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Resources from './views/Resources.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/resources',
      name: 'resources',
      component: Resources
    }
  ]
})
